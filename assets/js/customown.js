
if($(".asspanelinput .workerchoose .listworker").length > 0){
	$(document).on("click",".asspanelinput .workerchoose .listworker li a",function(){
		namerplc = $(this).attr("data-usname");
		$(this).closest(".workerchoose").find(".userassg").val(namerplc);
	});
}
$(document).ready(function(){
	if($(".tblpca").length > 0){
		wtblpca	= $(".tblpca").width();
		wtblpcapos = $(".tblpca").offset();
		btnremdiv = $(".tblpca .fieldtbldet button.removepca").offset();
		panelleftpos	= btnremdiv.left - wtblpcapos.left;
		$(".tblpca .fieldtbldet button.removepca").click(function(){
			$(this).closest(".fieldtbldet").find(".paneltbldet .ptbldetarr").css("width",wtblpca+"px");
			$(this).closest(".fieldtbldet").find(".paneltbldet .ptbldetarr").css("left","-"+panelleftpos+"px");
		});
	}
    if($(".filejobreqarea").length > 0){
        $(".filejobreqarea").bind('contextmenu', function (e) {
            idtabcusfol = $(this).closest(".tabscustomfol").attr("id");
            filelink = $(this).attr("data-linkfile");
            posfilelist = $(this).offset();
            leftposfile  = posfilelist.left + 30;
            topposfile = posfilelist.top + 20;

            $("#filerightclick").attr("data-id-ftab",idtabcusfol).attr("data-id-flink",filelink).css({
                "top" : topposfile+"px",
                "left" : leftposfile+"px",
                "visibility" : "visible",
                "opacity" : 1
            });
          
          // disable default context menu
          return false;
         });
    }
    $("#filerightclick ul li").click(function(){
        gettarget = $(this).attr("data-target");
        if(gettarget == "comment"){
            idtabcusfol = $(this).closest("#filerightclick").attr("data-id-ftab");
            idtabcusflink = $(this).closest("#filerightclick").attr("data-id-flink");
            $("#"+idtabcusfol+" ul.uk-tab li").each(function(){
                $(this).removeClass("uk-active").attr("aria-expanded",false);
            });
            $("#"+idtabcusfol+" ul.uk-tab li.tabcomment").addClass("uk-active").attr("aria-expanded",true);
            $("#"+idtabcusfol+" ul.tabfillarea li").each(function(){
                $(this).removeClass("uk-active").attr("aria-hidden",false);
            });
            $("#"+idtabcusfol+" ul.tabfillarea li.tabfillcomment").addClass("uk-active").attr("aria-hidden",true);
            $("#"+idtabcusfol+" ul.tabfillarea li.tabfillcomment .sendcomment .addedfilecmt").html('<span class="fileatcmt">'+idtabcusflink+'</span>');
            $("#"+idtabcusfol+" ul.tabfillarea li.tabfillcomment .sendcomment textarea.commentinp").focus();
            
        }
        $("#filerightclick").css({
            "opacity" : 0,
            "visibility" : "hidden",
        });
    });
    if($(".choosestatus").length > 0){
        $(".choosestatus > button").click(function(){
            if($(this).closest(".choosestatus").hasClass("opstaarea")){
                $(this).closest(".choosestatus").removeClass("opstaarea");
            }else{
                $(this).closest(".choosestatus").addClass("opstaarea");
            }
        });
        $(".choosestatus ul li.cardstatusnote a").click(function(){
            if($(this).closest("li").hasClass("sopennote")){
                $(this).closest("li").removeClass("sopennote");
            }else{
                $(this).closest("li").addClass("sopennote");
            }
        });
        $(".notearea .closenote").click(function(){
            $(this).closest("li").removeClass("sopennote");
        });
    }
    $(".settojs input + ins").click(function(){
        gvaltype    = $(this).closest("span.icheck-inline").find("input").attr("data-file-closing");
        if(gvaltype == 1){
            $(this).closest("span.icheck-inline").find(".barinfoclose").addClass("openinf");
            $(".sclosingfile").each(function(){
                $(this).show();
            });
        }else{
            $(".sclosingfile").each(function(){
                $(this).hide();
            });
            $(".barinfoclose").removeClass("openinf");
        }
    });
    $(".settojs label.labelrad").click(function(){
        gvaltype    = $(this).closest("span.icheck-inline").find("input").attr("data-file-closing");
        if(gvaltype == 1){
              $(this).closest("span.icheck-inline").find(".barinfoclose").addClass("openinf");
            $(".sclosingfile").each(function(){
                $(this).show();
            });
        }else{
            $(".sclosingfile").each(function(){
                $(this).hide();
            });
            $(".barinfoclose").removeClass("openinf");
        }
    });
	if($(".jobreqtitlekanban").length > 0){
        $(".jobreqtitlekanban").click(function (e) {
			contentkanban = $(this).closest(".content").find(".cntkanban").html();
			prioritykanban = $(this).closest(".content").find(".prckanban").html();
			precentkanban = $(this).closest(".content").find(".prckanban").attr("data-precent");
            /*tskpgcontent = $(".taskpgcontent").offset();
			tskpgleft = tskpgcontent.left;
            postitlejr = $(this).offset();
            leftpostitlejr  = postitlejr.left - 130;
			if(leftpostitlejr < tskpgleft){
				leftpostitlejr = tskpgleft + 15;
			}
            toppostitlejr = postitlejr.top + 20;*/

            $("#jobreqcardarea").find(".conceptmtxt").html(contentkanban);
            $("#jobreqcardarea").find(".prioritytmtxt").html(prioritykanban);
            $("#jobreqcardarea").find(".prctaxt b").html(precentkanban);
			
			var modal=UIkit.modal("#modalreqtaskdetail");
			
			modal.show();
			/*
		   $("#jobreqcardarea").css({
                "top" : toppostitlejr+"px",
                "left" : leftpostitlejr+"px",
                "transform" : "scale(1)"
            });*/
          
          // disable default context menu
          return false;
         });
    }
	if($(".asgbtndet").length > 0){
        $(".asgbtndet").click(function (e) {
			contentkanban = $(this).closest("li").find(".content .titletskasgn").html();
			prioritykanban = $(this).closest("li").find(".content small").html();
            $("#assignmenttskdet").find(".conceptmtxt").html(contentkanban);
            $("#assignmenttskdet").find(".prioritytmtxt").html(prioritykanban);
			
			var modal=UIkit.modal("#modalasgdetail");
			
			modal.show();
			/*
            tskpgcontent = $(".assgnpgcontent").offset();
			tskpgleft = tskpgcontent.left;
            postitlejr = $(this).offset();
            leftpostitlejr  = postitlejr.left - 10;
			if(leftpostitlejr < tskpgleft){
				leftpostitlejr = tskpgleft + 15;
			}
            toppostitlejr = postitlejr.top + 30;

            $("#assignmenttskdet").css({
                "top" : toppostitlejr+"px",
                "left" : leftpostitlejr+"px",
                "transform" : "scale(1)"
            });
          */
          // disable default context menu
          return false;
         });
    }
    if($(".asgpbutton .btnopenpanel").length > 0){
        $(".asgpbutton .btnopenpanel").click(function(e){
            wdtasgncontent = $(".assgnpgcontent").width();
            assgncontent = $(".assgnpgcontent").offset();
			posright = wdtasgncontent + assgncontent.left;
            if($(this).hasClass("assopenpanel")){
				$(this).next(".asspanelcustom").css("right","unset");
                $(this).next(".asspanelcustom").removeClass("assopenpanel");
                $(this).removeClass("assopentrue");
            }else{
                $(this).next(".asspanelcustom").addClass("assopenpanel");
                $(this).addClass("assopentrue");
				/*gasspanelpos = $(this).next(".asspanelcustom").offset();
				gwidtasspanel = $(this).next(".asspanelcustom").width();
				gcekright = gasspanelpos.left + gwidtasspanel;
				console.log(gcekright+" "+posright);
				if(gcekright > posright){
					$(this).next(".asspanelcustom").css("right","0px");
				}*/
            }
        });
    }
	if($(".tlaccordion").length > 0){
        $(".tlaccordion h3.uk-accordion-title").click(function () {
			if($(this).hasClass("uk-active")){
				$(this).removeClass("uk-active");
				$(this).next(".uk-accordion-content").slideUp(300);
			}else{
				$(this).addClass("uk-active");
				$(this).next(".uk-accordion-content").slideDown(300);
			}
         });
    }
	if($(".table-click-active").length > 0){
        $(".table-click-active tbody tr").click(function () {
			$(".table-click-active tbody tr").removeClass("list-active");
			$(this).addClass("list-active");
         });
        $(".table-click-active tbody tr *").click(function () {
			$(".table-click-active tbody tr").removeClass("list-active");
			$(this).closest("tr").addClass("list-active");
         });
    }
});
$(window).on('load', function() {
	
	if($("#approvaldet").length > 0){
		happdet	= $("#approvaldet").height();
		$("#approvaldet .previewfile").css("height",happdet+"px");
	}
    if($(".digasstabarea").length > 0){
        digassheight = $(".digasstabarea").innerHeight();
        $(".dignotearea").css("height",digassheight+"px");
    }
});

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev, target) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    document.getElementById(data).remove();
    var divdrop = target.id;
    posfollist = $("#"+divdrop).offset();
    leftposfol  = posfollist.left;
    topposfol  = posfollist.top + 10;
    $("#dropfileright").css({
        "top" : topposfol+"px",
        "left" : leftposfol+"px",
        "visibility" : "visible",
        "opacity" : 1
    });
    console.log(divdrop +" "+posfollist.left+" "+posfollist.top);
}
$(document).mouseup(function(e) 
{
    var fulbod = $("body");
    var container = $("#dropfileright");
    var asspanelcur = $(".asspanelcustom");
    var asscardnote = $(".cardstatusnote .notearea");
    var jobreqcardarea = $("#jobreqcardarea");
    var assignmenttskdet = $("#assignmenttskdet");
    var rightfilecl = $("#filerightclick");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.css({
            "opacity" : 0,
            "visibility" : "hidden",
        });
    }
    if ((!asspanelcur.is(e.target) && asspanelcur.has(e.target).length === 0) || (fulbod.is(e.target) && fulbod.has(e.target).length !== 0)) 
    {
        asspanelcur.closest(".asgpbutton").find(".btnopenpanel").removeClass("assopentrue");
        asspanelcur.removeClass("assopenpanel");
		//asspanelcur.css("right","unset");
    }
    if (!asscardnote.is(e.target) && asscardnote.has(e.target).length === 0) 
    {
        asscardnote.closest(".cardstatusnote").removeClass("sopennote");
    }
	/*
    if (!jobreqcardarea.is(e.target) && jobreqcardarea.has(e.target).length === 0 && !container.is(e.target) && container.has(e.target).length === 0 && !rightfilecl.is(e.target) && rightfilecl.has(e.target).length === 0) 
    {
        jobreqcardarea.css("transform","scale(0)");
    }*/
	/*
    if (!assignmenttskdet.is(e.target) && assignmenttskdet.has(e.target).length === 0) 
    {
        assignmenttskdet.css("transform","scale(0)");
    }*/
});

$(document).mousedown(function(e) 
{

    var rightfilecl = $("#filerightclick");
    if (!rightfilecl.is(e.target) && rightfilecl.has(e.target).length === 0) 
    {
        rightfilecl.css({
            "opacity" : 0,
            "visibility" : "hidden",
        });
    }
});
$("#dropfileright .uk-dropdown ul li").click(function(){
    $("#dropfileright").css({
        "opacity" : 0,
        "visibility" : "hidden",
    });
});
/* timesheet js */
$(document).ready(function(){
	var d = new Date(),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	datetoday = [year, month, day].join('/');
	$("[data-tishe-countdown]").each(function(){
		hourdate = $(this).attr('data-tishe-countdown');
		$(this).closest("td").next("td").html("<div class='timercdarea'></div>");
		var $this = $(this).closest("td").next("td").find(".timercdarea");
		finalDate = datetoday+" "+hourdate;
		console.log(finalDate);
		$this.countdown(finalDate, function(event) {
			$this.html('<span class="txtcdinfo">'+event.strftime('%H:%M:%S')+'</span>');
		});
	});
	$("[data-tishe-countdown]").hover(function(){
		$(this).closest("td").next("td").find(".timercdarea").show();
	});
	$("[data-tishe-countdown]").mouseleave(function(){
		$(this).closest("td").next("td").find(".timercdarea").hide();
	});
	$(".tsperiodchange").click(function(){
		if($(this).hasClass("dayviewed")){
			$(this).removeClass("dayviewed");
			$(this).text("This Week");
			$(".tbltimesheet thead .tisheday1 a.iconnext").remove();
			displaytsd	= "table-cell";
		}else{
			$(this).addClass("dayviewed");
			$(this).text("Day");
			$(".tbltimesheet thead .tisheday1").prepend('<a href="javascript:;" class="iconnext uk-icon-angle-right"></a>');
			displaytsd	= "none";
		}
		for(tsd=2;tsd<=7;tsd++){
			$(".tisheday"+tsd).each(function(){
				$(this).css("display",displaytsd);
			});
		}
	});
});
$(document).on("click",".iconnext",function(){
	$(this).closest("table").find("thead .tisheday1 a.iconnext").remove();
	$(".tsperiodchange").removeClass("dayviewed");
	$(".tsperiodchange").text("This Week");
	for(tsd=2;tsd<=7;tsd++){
		$(".tisheday"+tsd).each(function(){
			$(this).css("display","table-cell");
		});
	}
})
$(document).on("click",".headdateinf",function(){
	var dayNameArr = new Array ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	gyearspe	= $("#rnyear").val();
	gtdsdate	= $(this).attr("data-damo");
	gdatespe	= gyearspe+"/"+gtdsdate;
	thishtmlval	= $(this).html();
	$("table.tbltimesheet thead .tisheday1 .headdateinf").attr("data-damo",gtdsdate);
	$("table.tbltimesheet thead .tisheday1 .headdateinf").html(thishtmlval);
	$(".tisheday1 input").each(function(){
		$(this).val("");
	});
	for(tdsr=2;tdsr<=7;tdsr++){
		var currentDate = new Date(new Date(gdatespe).getTime() + 24 * 60 * 60 * 1000);
		var daynm = currentDate.getDay();
		var day = currentDate.getDate();
		var month = currentDate.getMonth() + 1;
		var year = currentDate.getFullYear();
		gdatespe	= gyearspe+"/"+month+"/"+day;
		$("table.tbltimesheet thead .tisheday"+tdsr+" .headdateinf").attr("data-damo",month+"/"+day);
		$("table.tbltimesheet thead .tisheday"+tdsr+" .headdateinf").html(dayNameArr[daynm]+"<br>"+day+" "+monthNames[month-1]);
		$(".tisheday"+tdsr+" input").each(function(){
			$(this).val("");
		});
	}
})